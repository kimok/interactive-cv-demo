(ns interactive-cv.defer
  (:require [reagent.core :as r]
            [cljs.core.async :refer [<! timeout]])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def deferral (r/atom 0))

(def defer? (r/reaction (> @deferral 100)))

(defn defer!
  ([] (defer! 1000))
  ([t]
   (if @defer?
     (swap! deferral #(+ t %))
     (go (reset! deferral t)
         (while @defer?
           (swap! deferral #(- % 100))
           (<! (timeout 100)))))))

(defn placeholder [v] (if @defer? [:<>] v))
