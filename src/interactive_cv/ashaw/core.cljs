(ns interactive-cv.ashaw.core
  (:require [interactive-cv.core :as cv]
            [interactive-cv.demo.core :as demo]
            [interactive-cv.route :as route]
            interactive-cv.ashaw.component
            interactive-cv.element
            [interactive-cv.org-edn :as org]
            [shadow.resource :refer [inline]]))

(inline "content/ashaw/content.org")

(defn forward []
  [:div
   (when-let [t @cv/next-route]
     [:div "→ " [:a {:href t} (route/slug->str t)]])])

(defn root-component []
  [:div {:style {:display          "flex"
                 :justify-content  "center"
                 :min-height       "100vh"
                 :font-family      "Garamond, Georgia, serif"
                 :opacity          (if (:once @cv/state) 1 0)
                 :background-color (:background-color @cv/context)
                 :transition       (str "background-color " "1s"
                                        ", "
                                        "opacity " (:transition-time @cv/context))}}
   [:div {:style {:width            "800px"
                  :margin-top       "4rem"
                  :margin-bottom    "4rem"
                  :box-shadow       (str (:shadow-x @cv/context) "rem "
                                         (:shadow-y @cv/context) "rem "
                                         "0.2rem rgba(0,0,0,0.5)")
                  :max-height       "60%"
                  :background-color (:foreground-color @cv/context)
                  :transition       (str "background-color " (:transition-time @cv/context)
                                         ", "
                                         "box-shadow " (:transition-time @cv/context))
                  :display          "flex"
                  :padding          "8% 4%"
                  :flex-direction   "column"}}
    [demo/top-nav]
    (some-> @cv/page org/zipper org/render)]])

(defn init []
  (cv/fetch-content
   {:on-response #(set! js/document.title (:title @cv/org-header))})
  (cv/mount root-component))
