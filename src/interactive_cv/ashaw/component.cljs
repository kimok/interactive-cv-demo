(ns interactive-cv.ashaw.component
  (:require [clojure.string :as str]
            [clojure.zip :as z]
            [interactive-cv.core :as cv]
            [interactive-cv.zip :as cv.zip]
            [interactive-cv.route :refer [route]]
            [interactive-cv.component :as c]
            [interactive-cv.org-edn :as org
             :refer [component]]
            [gnu.org-mode :as-alias org-mode]
            [garden.core :refer [css]]
            [garden.units :refer [percent]]
            [garden.stylesheet :refer [at-keyframes]]))

(defmethod component ::c/COLORTITLE [node]
  (-> (org/loc node)
      (z/edit org/with-type :div)
      z/down
      (z/edit org/with-type ::c/COLORTITLE.title)
      z/up))

(defmethod component ::c/COLORTITLE.title [_]
  (let [root-route? (= @route @cv/root-route)
        colorwords
        (into [:h2
               {:style
                {:position "relative"
                 :top (if (:once @cv/state) 0 -50)
                 :transition (:transition-time @cv/context)
                 :font-weight "normal"
                 :margin-bottom "4rem"}}]
              (map #(do [:a {:href @cv/root-route
                             :style {:color %1}} %2])
                   (str/split (:palette @cv/context) #" ")
                   (str/split (:title @cv/context) #" ")))]
    (cond-> colorwords
      root-route?
      (vary-meta assoc :drop-children true)
      (not root-route?)
      (into [" :: "]))))

(defmethod component ::c/LINKLIST [node]
  (-> (:loc (meta node))
      (z/edit org/with-type :div)
      (org/seek-descendant ::org-mode/list)
      (z/edit org/with-type ::LINKLIST.links)))

(defmethod component ::LINKLIST.links [node]
  (into  ^:drop-children ^:dbg

   [:div {:style {:display "flex"}}]
         (->> (org/children node)
              (map #(org/with-type % ::LINKLIST.item)))))

(defmethod component ::LINKLIST.item [node]
  (let [loc (:loc (meta node))
        tag (org/some-node loc ::org-mode/tag)
        link (org/some-node loc ::org-mode/link)
        image (org/some-node loc ::org-mode/link.file)]
    ^:drop-children ^:dbg
    [:a {:href (org/prop link :path)
         :style {:max-width "20%"
                 :margin-top (* 200 (rand))
                 :transition "margin-top 10s"
                 :min-height 200
                 :display "flex"
                 :flex-direction "column"
                 :align-content "center"}} image
     (org/with-type tag :div)]))

(defmethod component ::c/SIDEIMAGE [node]
  (let [loc (org/loc node)
        cover (org/some-node loc ::org-mode/link.file)
        content (some-> loc
                        (z/edit org/update-props assoc ::origin true)
                        (org/seek-descendant ::org-mode/link.file)
                        z/remove
                        (cv.zip/unseek (comp ::origin org/props))
                        (z/edit org/update-props dissoc ::origin)
                        z/children)]
    ^:drop-children
    [:div {:style {:display "flex"
                   :width "100%"
                   :justify-content "center"
                   :flex-wrap "wrap"
                   :flex-direction "row"
                   :margin-bottom "4rem"}}
     [:div {:style {:max-width "90%"
                    :display "flex"}}
      [:div {:style {:max-width "30%"}} cover]
      [:div {:style {:flex 1
                     :margin-left "2rem"}}
       (into [:div {:style {:text-align "center"
                            :justify-content "center"
                            :width "100%"}}]
             content)]]]))

(defmethod component ::c/FADECAROUSEL [node]
  (let [children (->> node org/children rest (filter org/org-element?))
        step (int (/ (:seconds @cv/state) 2))
        steps (range (count children))
        place-state (get-in @cv/state [node :place])
        running? (not (some? place-state))
        place (or place-state (nth (cycle steps) step) 0)
        bullet (fn [i]
                 [:a {:href "#"
                      :on-click
                      #(doto cv/state
                         (swap! update node assoc :place i)
                         (swap! assoc (nth children i) true))
                      :style {:display "block"}}
                  (if (#{i} place) "●" "○")])]
    ^:drop-children
    [:div {:style {:display "flex"
                   :width "100%"}}
     [:style (css (at-keyframes "fade-carousel"
                                [(percent 0) {:opacity 0}]
                                [(percent 10) {:opacity 1}]
                                [(percent 80) {:opacity 1}]
                                (when running?
                                  [(percent 100) {:opacity 0}])))]
     (into [:div] (map bullet) steps)
     [:div {:style {:width "100%"}
            :on-click #(swap! cv/state update node assoc :place place)}
      [:div {:key place
             :style {:animation "fade-carousel 2s linear"
                     :animation-fill-mode "forwards"
                     :width "100%"
                     :height "100%"}}
       (nth (cycle children) place)]]]))

(defmethod component ::c/EXPANDER [node]
  (let [loc (org/loc node)
        cover (org/some-node loc ::org-mode/link.file)
        content (some-> loc
                        (z/edit org/update-props assoc ::origin true)
                        (org/seek-descendant ::org-mode/link.file)
                        z/remove
                        (cv.zip/unseek (comp ::origin org/props))
                        (z/edit org/update-props dissoc ::origin)
                        z/children)]
    ^:drop-children
    [:div {:style {:display "flex"
                   :width "100%"
                   :justify-content "center"
                   :flex-wrap "wrap"
                   :flex-direction "row"
                   :margin-bottom "4rem"}}
     [:div {:style {:max-width "90%"
                    :display "flex"
                    :justify-content "center"}}
      [:a {:href "#"
           :on-click #(swap! cv/state update node not)
           :style {:flex 0.5}}
       cover]
      (when (get @cv/state node)
        [:div {:style {:flex 1
                       :margin-left "2rem"}}
         (into [:div {:style {:text-align "center"
                              :justify-content "center"
                              :width "100%"}}]
               content)])]]))
