(ns interactive-cv.element
  (:require [interactive-cv.org-edn :refer [element]]))

(defmacro defel [dispatch-val arg-binding-form & body]
  `(do
     (derive ~dispatch-val (keyword "gnu.org-mode" (name ~dispatch-val)))
     (defmethod element ~dispatch-val ~arg-binding-form ~@body)))
