(ns interactive-cv.doc)

(def docs (atom {}))

(defn add [k s] (swap! docs assoc k s))
