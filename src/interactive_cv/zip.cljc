(ns interactive-cv.zip
  (:require [clojure.zip :as z])
  (:refer-clojure :exclude [drop transduce]))

(defn top [loc]
  (->> loc
       (iterate z/up)
       (take-while some?)
       last))

(defn walk [loc f & args]
  (loop [loc loc]
    (if (z/end? loc)
      (z/root loc)
      (recur (z/next (apply f loc args))))))

(defn drop [loc pred]
  (cond-> loc (pred (z/node loc)) z/remove))

(defn root? [z]
  (nil? (z/up z)))

(defn transduce [loc stage end? pred]
  (->> (iterate stage loc)
       (take-while (complement end?))
       (reduce #(when (pred (z/node %2))
                  (reduced %2))
               loc)))

(defn seek [loc pred]
  (transduce loc z/next z/end? pred))

(defn unseek [loc pred]
  (transduce loc z/prev root? pred))

(defn child-locs [loc]
  (->> (z/down loc)
       (iterate z/right)
       (take-while some?)))

(defn seek-in [loc pred]
  (let [end-loc (loop [p loc]
                  (if (z/up p)
                    (or (z/right p) (recur (z/up p)))
                    [(z/node p) :end]))]
    (transduce loc z/next #{end-loc} pred)))
